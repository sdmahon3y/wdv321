// JavaScript Document

	var recipe1 = {
		name:"Crockpot Chili",
		image:"<img src='images/6697255305_bae7389e6f_b.jpg' alt='Crockpot Chili'>",
		servings:6,
		preperation_time:"25 Minutes",
		cooking_time:"6 Hours",
        difficulty:"Medium",
		ingredients: [{value: 2, measurement:"tbsp.", ingredient: "cooking oil"},
                      {value:1, measurement:"cup", ingredient: "onion",},
                      {value:1, measurement:"cup", ingredient:"chopped peppers"},
                      {value:4, measurement:"tbsp.", ingredient:"chili powder",},
                      {value:1, measurement:"tbsp.", ingredient:"hot chili powder(optional)"},
                      {value:1, measurement:"lb", ingredient:"ground beef or chicken"},
                      {value:2, measurement:"can", ingredient:"red beans"},
                      {value:2, measurement:"can", ingredient:"kidney beans"},
                      {value:2, measurement:"can", ingredient:"tomate puree"},
                      {value:2, measurement:"can", ingredient:"tomato sauce"},
                      {value:1, measurement:"cup", ingredient:"shredded cheese(optional)"},
                      {value:0.5, measurement:"cup", ingredient:"sour cream(optional)"}],
		instructions: ["Heat cooking oil in 2 quart skillet.","Saute onions and peppers for 5 minutes.","Add spices for 30 seconds.","Add meat and cook until browned. Approximately 15 minutes.","Pour contents of skillet into 3 quart crock pot.","Rinse beans and place in crockpot.","Open and pour Tomato puree and sauce into crock pot.","Cover crock pot and cook on low for 6 hours.","Serve into individual bowls and top with sour cream and cheese."],
	};
	
	var recipe2 = {
		name:"Indian Chicken Curry",
		image:"<img src='images/410873106_de3fe815df_b.jpg' alt='Chicken Curry'>",
		servings:4,
		preperation_time:"20 Minutes",
		cooking_time:"25 Minutes",
        difficulty:"Medium",
		ingredients: [{value:3, measurement:"tbsp.", ingredient:"olive oil"},
                      {value:1, measurement:"cup", ingredient:"onion"},
                      {value:2, measurement:"clove", ingredient:"garlic, minced"},
                      {value:3, measurement:"tbsp.", ingredient:"chili powder"},
                      {value:1, measurement:"teaspoon", ingredient:"ground cinnamon"},
                      {value:1, measurement:"teaspoon", ingredient:"paprika"},
                      {value:1, measurement:"",ingredient:"bay leaf"},
                      {value:0.5, measurement:"teaspoon", ingredient:"grated fresh ginger root"},
                      {value:0.5, measurement:"teaspoon", ingredient:"white sugar"},
                      {value:"1", measurement:"pinch", ingredient:"salt to taste"},
                      {value:2, measurement:"", ingredient:"skinless, boneless chicken breast half - cut into into bite-size pieces"},
                      {value:1, measurement:"tablespoon", ingredient:"tamato paste"},
                      {value:1, measurement:"cup", ingredient:"plain yogurt"},
                      {value:0.75, measurement:"cup", ingredient:"coconut milk"},
                      {value:0.5, measurement:"", ingredient:"lemon, juiced"},
                      {value:0.5, measurement:"teaspoon", ingredient:"cayenne pepper"}],
		instructions: ["Heat olive oil in a skillet over medium heat.","Saute onion until lightly browned.","Stir in garlic, curry powder, cinammon, paprika, bay leaf, ginger, sugar and salt.","Continue stirring for 2 minutes.","Add chicken pieces, tomato paste, yogurt, and coconut milk.","Bring to a boil, reduce heat, and simmer for 20 to 25 minutes.","Remove bay leaf, and stir in lemon juice and cayenne pepper. Simmer 5 more minutes."]
	}
    
    var recipe3 = {
        name:"Tattertot Casserole",
        image:"<img src='images/5187974768_3be8858bd9_b.jpg' alt='Tattertot Casserole'>",
        servings:4,
        preperation_time:"10 Minutes",
        cooking_time:"30 Minutes",
        difficulty:"Easy",
        ingredients: [{value:1, measurement:"pound", ingredient:"ground beef"},
                      {value:1, measurement:"pinch", ingredient:"salt and ground black pepper"},
                      {value:1, measurement:"can", ingredient:"condensed cream of mushroom soup"},
                      {value:2, measurement:"cup", ingredient:"shredded cheese"},
                      {value:16, measurement:"ounce", ingredient:"tater tots"}],
        instructions:["Preheat oven to 350 degrees F (175 degrees C).","Cook and stir ground beef in a large skillet over medium heat until no longer pink and completely browned, 7 to 10 minutes; season with salt and black pepper.","Stir cream of mushroom soup into the cooked ground beef; pour the mixture into a 9x13-inch baking dish.","Layer tater tots evenly over the ground beef mixture; top with Cheddar cheese.","Bake until tater tots are golden brown and hot, 30 to 45 minutes."]
    }
	
	var recipes = [recipe1, recipe2, recipe3];
	//console.log(recipes);

	function unorderedClick() {
		$("ul li").click(function() { //Add onClick event to all list elements within unordered list
			if($(this).hasClass("click")) { //Check if element currently has class
				$(this).removeClass("click"); //Remove class
				}
				else {
				$(this).addClass("click"); //Add class
				}			   
		});
	}

	function orderedClick() {
		$("ol li").click(function() { //Add onClick event to all list elements within unordered list
			if($(this).hasClass("click")) { //Check if element currently has class
				$(this).removeClass("click"); //Remove class
				}
				else {
				$(this).addClass("click"); //Add class
				}			   
		});
	};

    $(document).ready(function(){
     
    var recipeSelect = ""; 
    var recipeOptions = "";
        
       for(var x = 0; x < recipes.length; x++) //For loop
       {
           let recipeName = recipes[x].name; //Creates variable for current state
           let opt = "<option value=" + x + ">" + recipeName + "</option>"; 
           //console.log(opt);
           var recipeOptions = recipeOptions.concat(opt); //Creates a list of all the options
       }
        
        var recipeSelect = "<label for='recipes' class='bold'>Choose: </label><select name='recipes' id='recipes'>" + recipeOptions + "</select>";
        //console.log(recipeSelect);
        document.querySelector('#div1').innerHTML = recipeSelect;
        document.querySelector('#image').innerHTML = recipes[0].image;
        document.querySelector('#servings').innerHTML = "Servings: " + recipes[0].servings;
        document.querySelector('#preparation').innerHTML = "Preperation Time: " + recipes[0].preperation_time;
        document.querySelector('#cooking').innerHTML = "Cooking Time: " + recipes[0].cooking_time;
        document.querySelector('#difficulty').innerHTML = "Difficulty: " + recipes[0].difficulty; //Generate HTML for default recipe
        
        $("#recipes").change(function() {
            let x = $(this).children("option:selected").val();
            document.querySelector('#image').innerHTML = recipes[x].image;
            document.querySelector('#servings').innerHTML = "Servings: " + recipes[x].servings;
            document.querySelector('#preparation').innerHTML = "Preperation Time: " + recipes[x].preperation_time;
            document.querySelector('#cooking').innerHTML = "Cooking Time: " + recipes[x].cooking_time;
            document.querySelector('#difficulty').innerHTML = "Difficulty: " + recipes[x].difficulty; //Generate HTML on recipe change
            
            let parentDiv = document.getElementById("div2");
            
            if (parentDiv.hasChildNodes()) {
                document.querySelector("#div2").innerHTML = ""; //Clear div on recipe change
                //console.log("Cleared");
                }
            
            let parentDiv2 = document.getElementById("div3");
            
            if (parentDiv2.hasChildNodes()) {
                document.querySelector("#div3").innerHTML = ""; //Clear div on recipe change
                //console.log("Cleared");
                };
            });
        });
    
    function getIngredients(){
                 
        //console.log("Button Clicked");
        
        let parentDiv = document.getElementById("div2");
        
        if (parentDiv.hasChildNodes()) {
            document.querySelector("#div2").innerHTML = ""; //Clear div if it has children
            //console.log("Cleared");
        }
        
        else {
        
        var radioButtons = "<h3>Serving Size:</h3><p>Half <input type='radio' name='serving' value='0.5'></p><p>Default <input type='radio' name='serving' value='1' checked='true'></p><p>Double <input type='radio' name='serving' value='2'</p>" //Build Radio Buttons
        
        var unorderedList = "<ul name='ingredients' value='ingredients' id='ingredients'></ul>" //Build unordered list
        
        document.querySelector("#div2").innerHTML = radioButtons + unorderedList; //Generate HTML for radio buttons and unordered list
        
        $(":radio").change(function() {
            
                var servings = "";
                var ingredientList = "";
            
                var servings = parseFloat($('input[name=serving]:checked').val());
            
                let x = $("#recipes").children("option:selected").val();
				var servingSize = recipes[x].servings;
				var servingSize = servingSize * servings;
				document.querySelector('#servings').innerHTML = "Servings: " + servingSize;
        
                for(var y = 0; y < recipes[x].ingredients.length; y++)
                {
                let value = parseFloat(recipes[x].ingredients[y].value);
                let newValue = value * servings;
                let measurement = recipes[x].ingredients[y].measurement;
                let ingredient = recipes[x].ingredients[y].ingredient;
                let list = "<li>" + newValue + " " + measurement + " " + ingredient + "</li>"; 
                        
                var ingredientList = ingredientList.concat(list); //Creates list elements for each ingredient                 
            }
        
            //console.log(ingredientList);
            document.querySelector("#ingredients").innerHTML = ingredientList //Put list elements inside unordered list
        	
			unorderedClick(); //call function
            });
        
        var servings = "";
        var ingredientList = "";
            
        var servings = parseFloat($('input[name=serving]:checked').val()); //Find value of currently selected radio button
            
        let x = $("#recipes").children("option:selected").val(); //Find value of currently selected recipe
        
        for(var y = 0; y < recipes[x].ingredients.length; y++)
            {
            let value = parseFloat(recipes[x].ingredients[y].value);
            let newValue = value * servings; //Multiply value by radio button value
            let measurement = recipes[x].ingredients[y].measurement;
            let ingredient = recipes[x].ingredients[y].ingredient;
            let list = "<li>" + newValue + " " + measurement + " " + ingredient + "</li>"; 
            var ingredientList = ingredientList.concat(list); //Creates list elements for each ingredient     
            };
        
        document.querySelector("#ingredients").innerHTML = ingredientList; //Put list elements inside unordered list
		
		unorderedClick(); //call function
        };
		
    };

	function getInstructions() {
    
    //console.log("Button Clicked");
    
    let parentDiv = document.getElementById("div3");
    
    if (parentDiv.hasChildNodes()) {
            document.querySelector("#div3").innerHTML = ""; //Clear div if it has children
            //console.log("Cleared");
        }
        
    else {     
        var instructionList = "";
        
        let x = $("#recipes").children("option:selected").val(); //Find value of currently selected recipe
        //console.log(x);
            
        for(var y = 0; y < recipes[x].instructions.length; y++)
            {
            let text = recipes[x].instructions[y];
            let list = "<li>" + text + "</li>";
            var instructionList = instructionList.concat(list); //Creates list elements for each instruction  
            //console.log(instructionList);
            };
            
        document.querySelector("#div3").innerHTML = "<ol>" + instructionList + "</ol>";
        };
		
		orderedClick(); //call function
    };
    
$(document).ready(function(){
document.getElementById('button').addEventListener('click', getIngredients);
document.getElementById('button2').addEventListener('click', getInstructions);
});