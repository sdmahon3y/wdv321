<?php 
    // lolliPop
	$lolliPop = new stdClass();
	$lolliPop->flavor = 'Cherry';
	$lolliPop->description = 'Coin Shape';
	$lolliPop->price = '$1.29';
    $lolliPop->image = "<img src='images/LolliPop.jpg' alt='LolliPop'>";
    
    // Candy Corn
    $candyCorn = new stdClass();
	$candyCorn->flavor = 'Lemon and Orange';
	$candyCorn->description = 'Triangle Shaped';
	$candyCorn->price = '$1.89';
    $candyCorn->image = "<img src='images/candyCorn.jpg' alt='Candy Corn'>";
  
	// Array that holds each object
	$arrayOfCandy = array();
	// Adding objects to array
	array_push($arrayOfCandy, $lolliPop, $candyCorn);
	
	$candyObject = new stdClass();
	$candyObject->name = "candyObj";
	$candyObject->candy = $arrayOfCandy;

	$myJSON = json_encode($candyObject);	// Converting to JSON
	echo $myJSON;
	$my_file = $candyObject->name . ".js";
	$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
	$data = $myJSON;
	fwrite($handle, $data);
?>




