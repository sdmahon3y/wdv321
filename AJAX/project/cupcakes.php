<?php 
    // Vanilla Cupcake
	$vanillaCupcake = new stdClass();
	$vanillaCupcake->flavor = 'vanilla';
	$vanillaCupcake->description = 'Vanilla flavored cupcake with vanilla frosting.';
	$vanillaCupcake->price = '$1.29';
    $vanillaCupcake->image = "<img src='images/vanilla.jpg' alt='Vanilla Cupcake'>";
    
    // Chocolate Cupcake
    $chocolateCupcake = new stdClass();
	$chocolateCupcake->flavor = 'chocolate';
	$chocolateCupcake->description = 'Chocolated flavored cupcake with vanilla frosting.';
	$chocolateCupcake->price = '$1.39';
    $chocolateCupcake->image = "<img src='images/chocolate.jpg' alt='Chocolate Cupcake'>";

    // Red Velvet Cupcake
    $redVelvetCupcake = new stdClass();
	$redVelvetCupcake->flavor = 'redVelvet';
	$redVelvetCupcake->description = 'Red velvet flavored cupcake with vanilla frosting.';
	$redVelvetCupcake->price = '$1.49';
	$redVelvetCupcake->image = "<img src='images/redvelvet.jpg' alt='Red Velvet Cupcake'>";
	
	// Array that holds each object
	$arrayOfCupcakes = array();

	// Adding objects to array
	array_push($arrayOfCupcakes, $vanillaCupcake, $chocolateCupcake, $redVelvetCupcake);
	
	$cupcakesObject = new stdClass();
	$cupcakesObject->name = "cupcakesObj";
	$cupcakesObject->cupcakes = $arrayOfCupcakes;


	$myJSON = json_encode($cupcakesObject);	// Converting to JSON
	echo $myJSON;

	$my_file = $cupcakesObject->name . ".js";
	$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
	$data = $myJSON;
	fwrite($handle, $data);
?>