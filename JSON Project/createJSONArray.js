// JavaScript Document
/*
	This file will:
	
	- Create a Javascript object containing an array of records with each record including an array
	- Convert the Javascript object into a JSON object
	- Store the JSON object into local storage
	
	This will be given to students to run as part of a web page that will consume the JSON object
	Goal: Provide an example of how to create a JSON object in JS
	Goal: Provide an example of how to consume a JSON object in JS

	Use the following data for this object:
	
		student_id = 332443
		student_gpa = 3.6
		student_courses = ["WDV101","WDV131","WDV105"]	

		student_id = 545467
		student_gpa = 2.7
		student_courses = ["WDV101","WDV131","WDV105","WDV221","WDV205"]	
		
		student_id = 128574
		student_gpa = 3.4
		student_courses = ["WDV101","WDV131","WDV105","WDV221","WDV205","WDV341"]	
	
		student_id = 750056
		student_gpa = 1.85
		student_courses = ["WDV101","WDV131","WDV105","WDV221","WDV205"]		
		
		
*/

var record1 = {
    student_id:332443,
    student_gpa:3.6,
    student_courses:["WDV101","WDV131","WDV105"]
};

var record2 = {
    student_id:5445467,
    student_gpa:2.7,
    student_courses:["WDV101","WDV131","WDV105","WDV221","WDV205"]
};

var record3 = {
    student_id:128574,
    student_gpa:3.4,
    student_courses:["WDV101","WDV131","WDV105","WDV221","WDV205","WDV341"]
};

var record4 = {
    student_id:750056,
    student_gpa:1.85,
    student_courses:["WDV101","WDV131","WDV105","WDV221","WDV205"]
};

var records = [record1,record2,record3,record4];
console.log(records);

localStorage.setItem('recordsJSON', JSON.stringify(records));	//turns object into a JSON object and stores it on the browser.