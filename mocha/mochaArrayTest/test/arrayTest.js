// JavaScript Document
//arrayTest.js

var assert = require('chai').assert;

var arr = [];

describe('Array', function() {
	
	it('should start empty', function() {
		
		//var arr = [];
		
		assert.equal(arr.length, 0);
	});
	
	
	it('should have five items', function() {
		
		//var arr = [1,2,3,4,5];
		
		assert.equal(arr.length, 5);
	});
	
	
});
